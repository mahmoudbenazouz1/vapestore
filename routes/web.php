<?php

use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [OrderController::class, 'create'])->name('boilerplate.orders.create');
Route::post('/orders/create', [OrderController::class, 'store'])->name('boilerplate.orders.store');
Route::group([
    'domain' => config('boilerplate.app.domain', ''),
    'middleware' => ['web', 'boilerplate.locale'],
    'as' => 'boilerplate.',
], function () {
    // Admin routes
    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('items', [ItemController::class, 'index'])->name('items.index');
        Route::post('items', [ItemController::class, 'store'])->name('items.store');
        Route::get('items/create', [ItemController::class, 'create'])->name('items.create');
        Route::get('items/{item}/edit', [ItemController::class, 'edit'])->name('items.edit');
        Route::put('items/{item}/update', [ItemController::class, 'update'])->name('items.update');
        Route::delete('items/{item}/destroy', [ItemController::class, 'destroy'])->name('items.destroy');

        Route::get('orders', [OrderController::class, 'index'])->name('orders.index');
        Route::get('orders/{order}/edit', [OrderController::class, 'edit'])->name('orders.edit');
        Route::delete('orders/{order}/destroy', [OrderController::class, 'destroy'])->name('orders.destroy');
    });

});
