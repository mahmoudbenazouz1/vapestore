<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class itemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(Request $request): array
    {
        $itemId = $request->route('item');
        return [
            'title' => ['required', 'string', Rule::unique('items')->ignore($itemId)],
            'description' => ['required', 'string'],
            'price' => ['required', 'numeric', 'regex:/^\d+(\.\d+)?$/'],
            'stock' => ['required', 'integer'],
            'image' => 'required|image|mimes:jpeg,png,gif',
        ];
    }
}
