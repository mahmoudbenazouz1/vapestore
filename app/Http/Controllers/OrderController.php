<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class OrderController extends Controller
{
    public function index()
    {
        return view('orders.list');
    }
    public function edit(Order $order)
    {
        return view('orders.edit', [
            'order' => $order,
        ]);
    }

    public function create()
    {
        $items = Item::where('stock', '>', 0)->get();
        return view('orders.create', [
            'items' => $items,
        ]);
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'items' => 'required',
        ]);

        $order = new Order();
        $order->client_name = $request->client_name;
        $order->client_phone = $request->client_phone;
        $order->save();

        $order->items()->attach($request->items);
        return redirect()->route('boilerplate.orders.create')
            ->with('growl', [__('boilerplate::orders.successadd'), 'success']);
    }

    public function destroy(Order $order)
    {
        return response()->json(['success' => $order->delete() ?? false]);
    }
}
