<?php

namespace App\Http\Controllers;

use App\Http\Requests\itemRequest;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ItemController extends Controller
{

    public function index()
    {
        return view('items.list');
    }


    public function create()
    {
        return view('items.create');
    }


    public function store(itemRequest $request)
    {
        $item = Item::create([
            "title" => $request->title,
            "description" => $request->description,
            "price" => $request->price,
            "stock" => $request->stock,
        ]);
        $item->image = $request->image->storeAs('public/items/' . $item->id . '_item', $item->id . '_item_' . $request->image->getClientOriginalName());
        $item->image = str_replace('public/', 'storage/', $item->image);
        $item->save();


        return redirect()->route('boilerplate.items.edit', $item)
            ->with('growl', [__('boilerplate::items.successadd'), 'success']);
    }




    public function edit(Item $item)
    {
        return view('items.edit', [
            'item' => $item,
        ]);
    }


    public function update(Request $request, Item $item)
    {
        $this->validate($request, [
            'title' => 'required|unique:items,title,' . $item->id,
            'description' => 'required|string',
            'price' => 'required|numeric|regex:/^\d+(\.\d+)?$/',
            'stock' => 'required|integer',
        ]);
        if (isset($request->image)) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,gif',
            ]);
            $oldImagePath = $item->image;
            if (File::exists($oldImagePath)) {
                File::delete($oldImagePath);
            }
            $item->image = $request->image->storeAs('public/items/' . $item->id . '_item', $item->id . '_item_' . $request->image->getClientOriginalName());
            $item->image = str_replace('public/', 'storage/', $item->image);
            $item->save();
        }


        $item->update([
            'title' => $request->title,
            "description" => $request->description,
            "price" => $request->price,
            "stock" => $request->stock,
        ]);

        return redirect()->route('boilerplate.items.edit', $item)
            ->with('growl', [__('boilerplate::items.successmod'), 'success']);
    }


    public function destroy(Item $item)
    {
        $directoryPath = storage_path('app/public/items/' . $item->id . '_item');

        if (File::exists($directoryPath)) {
            File::deleteDirectory($directoryPath);
        }
        return response()->json(['success' => $item->delete() ?? false]);
    }
}
