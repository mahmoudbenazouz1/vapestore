<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class OrderMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        $item = $menu->add('boilerplate::orders.title', [
            'permission' => 'users_crud',
            'icon' => 'square',
            'role' => 'admin',
            'order' => 1030,
        ]);


        $item->add('boilerplate::orders.list.title', [
            'route' => 'boilerplate.orders.index',
            'active' => 'boilerplate.orders.index,boilerplate.orders.edit',
            'order' => 1003,
        ]);
    }
}
