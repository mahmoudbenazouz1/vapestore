<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class ItemMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        $item = $menu->add('boilerplate::items.title', [
            'permission' => 'users_crud',
            'icon' => 'square',
            'role' => 'admin',
            'order' => 1030,
        ]);

        $item->add('boilerplate::items.create.title', [
            'route' => 'boilerplate.items.create',
            'order' => 1002,
        ]);

        $item->add('boilerplate::items.list.title', [
            'route' => 'boilerplate.items.index',
            'active' => 'boilerplate.items.index,boilerplate.items.edit',
            'order' => 1003,
        ]);
    }
}
