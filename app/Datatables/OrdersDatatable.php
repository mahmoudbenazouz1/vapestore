<?php

namespace App\Datatables;

use App\Models\Boilerplate\User;
use App\Models\Item;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;

class OrdersDatatable extends Datatable
{
    public $slug = 'orders';

    public function datasource()
    {

        $query = Order::query();
        return $query;
    }

    public function setUp()
    {
        $this->order('created_at');
    }

    public function columns(): array
    {
        return [
            Column::add(__('boilerplate::orders.client.name'))
                ->data('client_name'),
            Column::add(__('boilerplate::orders.client.phone'))
                ->data('client_phone'),

            Column::add()
                ->width('20px')
                ->actions(function (Order $order) {
                    return join([
                        // Button::show('plan.show', $plan),
                        Button::edit('boilerplate.orders.edit', $order),
                        Button::delete('boilerplate.orders.destroy', $order),
                    ]);
                }),
        ];
    }
}
