<?php

namespace App\Datatables;

use App\Models\Boilerplate\User;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Datatables\Button;
use Sebastienheyd\Boilerplate\Datatables\Column;
use Sebastienheyd\Boilerplate\Datatables\Datatable;

class ItemsDatatable extends Datatable
{
    public $slug = 'items';

    public function datasource()
    {

        $query = Item::query();
        return $query;
    }

    public function setUp()
    {
        $this->order('created_at');
    }

    public function columns(): array
    {
        return [
            Column::add(__('boilerplate::items.name'))
                ->data('title'),
            Column::add(__('boilerplate::items.description'))
                ->data('description'),
            Column::add(__('boilerplate::items.price'))
                ->data('price'),
            Column::add(__('boilerplate::items.stock'))
                ->data('stock'),
            Column::add(__('boilerplate::users.list.creationdate'))
                ->data('created_at')
                ->dateFormat(),

            Column::add()
                ->width('20px')
                ->actions(function (Item $item) {
                    return join([
                        // Button::show('plan.show', $plan),
                        Button::edit('boilerplate.items.edit', $item),
                        Button::delete('boilerplate.items.destroy', $item),
                    ]);
                }),
        ];
    }
}
