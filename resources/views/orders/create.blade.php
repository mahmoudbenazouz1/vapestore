@extends('boilerplate::auth.layout', ['title' => __('boilerplate::orders.title')])

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        /* styles.css */
        body {
            background-image: url("../assets/images/image_vape.jpg");
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            color: #fff;
        }

        .container {
            max-width: 600px;
            margin: 50px auto;
            padding: 20px;
            background-color: rgba(0, 0, 0, 0.8);
            border-radius: 10px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.8);
        }

        h1,
        h2 {
            text-align: center;
        }

        ul.item-list {
            padding: 0;
        }

        .item {
            margin-bottom: 20px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 8px;
            overflow: hidden;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .item:hover {
            background-color: rgba(255, 255, 255, 0.9);
        }

        .item.checked {
            border: 2px solid blue;
            /* Border style for checked items */
        }

        .item-label {
            display: block;
            padding: 10px;
        }

        .item-content {
            display: flex;
            align-items: center;
        }

        .item-image-container {
            flex-shrink: 0;
            margin-right: 15px;
        }

        .item-image {
            width: 50px;
            height: 50px;
            border-radius: 5px;
            object-fit: cover;
        }

        .item-details {
            flex-grow: 1;
        }

        .item-title {
            margin-bottom: 5px;
            font-size: 18px;
            font-weight: bold;
        }

        .item-stock {
            margin-bottom: 5px;
            font-size: 14px;
            color: #555;
        }

        .item-description {
            font-size: 14px;
            color: #333;
        }

        .item-checkbox {
            display: none;
        }

        button {
            display: block;
            margin: 20px auto;
            padding: 10px 20px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }

        .error-bubble {
            display: inline-block;
            background-color: #ffcccc;
            color: #ff0000;
            padding: 5px 10px;
            border-radius: 5px;
            font-size: 14px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <div class="container">
        <h1>Create Order</h1>
        @if(session()->has('growl'))
        <div class="alert alert-{{ session('growl')[1] }}">
            {{ session('growl')[0] }}
        </div>
        @endif

        <form action="{{ route('boilerplate.orders.store') }}" method="POST" id="surveyForm">
            @csrf
            <h2>Select Items</h2>
            @error('items')
            <div class="error-bubble">
                <div>{{$message}}</div>
            </div>
            @enderror
            <ul class="item-list">
                @foreach($items as $item)
                <input type="checkbox" id="item_{{ $item->id }}" name="items[]" value="{{ $item->id }}" class="item-checkbox">
                <li class="item">
                    <label for="item_{{ $item->id }}" class="item-label">
                        <div class="item-content">
                            <div class="item-image-container">
                                <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="item-image">
                            </div>
                            <div class="item-details">
                                <h3 class="item-title">{{ $item->title }}</h3>
                                <p class="item-stock">Stock: {{ $item->stock }}</p>
                                <p class="item-description">{{ $item->description }}</p>
                            </div>
                        </div>
                    </label>
                </li>
                @endforeach
            </ul>

            <button type="button" onclick="getClientNameAndSubmit()">Create Order</button>
        </form>
    </div>

    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->
    <script>
        $(document).ready(function() {
            $('.item').click(function() {
                $(this).toggleClass('checked');
                const checkbox = $(this).find('.item-checkbox');
                checkbox.prop('checked', !checkbox.prop('checked'));
            });

            // Prevent checkbox clicks from triggering item clicks
            $('.item-checkbox').click(function(event) {
                event.stopPropagation();
            });
        });

        function getClientNameAndSubmit() {
            const clientName = prompt('Enter client name:');
            const clientPhone = prompt('Enter client phone number:');

            if (clientName && clientName.trim() !== '' && clientPhone && clientPhone.trim() !== '') {
                $('<input>').attr({
                    type: 'hidden',
                    name: 'client_name',
                    value: clientName.trim()
                }).appendTo('#surveyForm');

                $('<input>').attr({
                    type: 'hidden',
                    name: 'client_phone',
                    value: clientPhone.trim()
                }).appendTo('#surveyForm');

                $('#surveyForm').unbind('submit').submit(); // Submit the form with client name and phone
            } else {
                alert('Please enter both client name and phone number.');
            }
        }
    </script>
</body>

</html>