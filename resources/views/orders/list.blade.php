@extends('boilerplate::layout.index', [
'title' => __('boilerplate::orders.title'),
'subtitle' => __('boilerplate::orders.list.title'),
'breadcrumb' => [
__('boilerplate::orders.list.title') => 'boilerplate.orders.index'
]
])

@section('content')

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'orders']) @endcomponent

@endcomponent
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush