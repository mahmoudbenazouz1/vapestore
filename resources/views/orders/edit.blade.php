@extends('boilerplate::layout.index', [
'title' => __('boilerplate::orders.title'),
'subtitle' => __('boilerplate::orders.edit.title'),
'breadcrumb' => [
__('boilerplate::orders.title') => 'boilerplate.orders.index',
__('boilerplate::orders.edit.title')
]
])
<style>
    .delete-icon {
        color: red;
        cursor: pointer;
        position: absolute;
        top: 5px;
        right: 5px;
    }

    .image-container {
        border: 1px solid #ccc;
        padding: 5px;
        position: relative;
        display: inline-block;
        width: 200px;
        /* Adjust the width if needed */
    }

    #image {
        width: 100%;
        height: auto;
    }
</style>
@section('content')
<div class="row">
    <div class="col-12 pb-3">
        <a href="{{ route("boilerplate.orders.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::orders.returntolist')">
            <span class="far fa-arrow-alt-circle-left text-muted"></span>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        @component('boilerplate::card', ['title' => 'boilerplate::orders.informations'])
        @component('boilerplate::input', ['name' => 'client_name', 'label' => 'boilerplate::orders.client.name','value'=> $order->client_name,'readonly'])@endcomponent
        @component('boilerplate::input', ['name' => 'client_phone', 'label' => 'boilerplate::orders.client.phone','value'=> $order->client_phone,'readonly'])@endcomponent
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->items as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->stock }}</td>
                    <td>
                        @if ($item->image)
                        <img id="image" src="{{ asset($item->image) }}" style="width: 100px;">
                        @else
                        No Image
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>


        @endcomponent
    </div>

</div>


@endsection