@extends('boilerplate::layout.index', [
'title' => __('boilerplate::items.title'),
'subtitle' => __('boilerplate::items.create.title'),
'breadcrumb' => [
__('boilerplate::items.title') => 'boilerplate.items.index',
__('boilerplate::items.create.title')
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.items.store') }}" autocomplete="off" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.items.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::items.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::items.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::items.informations'])
            @component('boilerplate::input', ['name' => 'title', 'label' => 'boilerplate::items.name'])@endcomponent
            @component('boilerplate::input', ['name' => 'description', 'label' => 'boilerplate::items.description'])@endcomponent
            @component('boilerplate::input', ['name' => 'price', 'label' => 'boilerplate::items.price'])@endcomponent
            @component('boilerplate::input', ['name' => 'stock', 'label' => 'boilerplate::items.stock'])@endcomponent
            <label for="avatar"> @lang('boilerplate::items.image')</label><br>
            @error('image')
            <div class="error-bubble">
                <div>{{$message}}</div>
            </div>
            @enderror
            <input type="file" id="avatar" name="image" accept="image/png, image/jpeg" />
            @endcomponent
        </div>
    </div>
</form>
@endsection