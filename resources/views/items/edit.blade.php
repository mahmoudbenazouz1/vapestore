@extends('boilerplate::layout.index', [
'title' => __('boilerplate::items.title'),
'subtitle' => __('boilerplate::items.edit.title'),
'breadcrumb' => [
__('boilerplate::items.title') => 'boilerplate.items.index',
__('boilerplate::items.edit.title')
]
])
<style>
    .delete-icon {
        color: red;
        cursor: pointer;
        position: absolute;
        top: 5px;
        right: 5px;
    }

    .image-container {
        border: 1px solid #ccc;
        padding: 5px;
        position: relative;
        display: inline-block;
        width: 200px; /* Adjust the width if needed */
    }

    #image {
        width: 100%;
        height: auto;
    }
</style>
@section('content')
<form action="{{ route('boilerplate.items.update', $item->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.items.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('boilerplate::items.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('boilerplate::items.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'boilerplate::items.informations'])
            @component('boilerplate::input', ['name' => 'title', 'label' => 'boilerplate::items.name','value'=> $item->title])@endcomponent
            @component('boilerplate::input', ['name' => 'description', 'label' => 'boilerplate::items.description','value'=> $item->description])@endcomponent
            @component('boilerplate::input', ['name' => 'price', 'label' => 'boilerplate::items.price','value'=> $item->price])@endcomponent
            @component('boilerplate::input', ['name' => 'stock', 'label' => 'boilerplate::items.stock','value'=> $item->stock])@endcomponent
            <label for="avatar"> @lang('boilerplate::items.image')</label><br>
            @if($item->image)
            <div class="image-container" id="image-container">
                <img id="image" src="{{ asset($item->image) }}" width="200">
                <span class="delete-icon" onclick="deleteImage(event)">❌</span>
                <input type="hidden" id="imageInput" name="file" value="{{ $item->image }}">
            </div>
            @error('image')
            <div class="error-bubble">
                <div>{{$message}}</div>
            </div>
            @enderror
            <input type="file" id="fileInput" name="image" accept="image/png, image/jpeg" style="display: none;">
            @endif
            @endcomponent
        </div>

    </div>
</form>

<script>
    function deleteImage(event) {
        event.preventDefault(); // Prevent form submission

        var imageContainer = document.getElementById('image-container');
        var deleteButton = document.getElementById('deleteButton');
        var fileInput = document.getElementById('fileInput');
        var imageInput = document.getElementById('imageInput');

        if (confirm('Are you sure you want to delete this image?')) {
            // Remove the image container
            imageContainer.parentNode.removeChild(imageContainer);

            // Show the file input field if it exists
            if (fileInput) {
                fileInput.style.display = 'block';
            }

            // Clear the value of the hidden input field
            imageInput.value = '';

            // Hide the delete button if it exists
            if (deleteButton) {
                deleteButton.style.display = 'none';
            }

            // Optionally, you can submit the form or perform other actions here
        }
    }
</script>

@endsection