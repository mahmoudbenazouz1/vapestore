@extends('boilerplate::layout.index', [
'title' => __('boilerplate::items.title'),
'subtitle' => __('boilerplate::items.list.title'),
'breadcrumb' => [
__('boilerplate::items.list.title') => 'boilerplate.items.index'
]
])

@section('content')
<div class="row">
    <div class="col-12 mbl">
        <span class="float-right pb-3">
            <a href="{{ route("boilerplate.items.create") }}" class="btn btn-primary">
                @lang('boilerplate::items.create.title')
            </a>
        </span>
    </div>
</div>

@component('boilerplate::card')

@component('boilerplate::datatable', ['name' => 'items']) @endcomponent

@endcomponent
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }
</style>
@endpush