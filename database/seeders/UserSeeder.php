<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userModel = config('auth.providers.users.model');
        $roleModel = config('laratrust.models.role');

        $admin_user = $userModel::withTrashed()->updateOrCreate(['email' => 'admin@iec-telecom.com'], [
            'active'     => true,
            'first_name' => 'Admin',
            'last_name'  => 'Vapestore',
            'email'      => 'admin@vapestore.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $admin = $roleModel::whereName('admin')->first();
        $admin_user->attachRole($admin);
    }
}
