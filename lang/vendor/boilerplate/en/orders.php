<?php

return [
    'title'         => 'Orders',
    'returntolist'  => 'Order list',
    'save'          => 'Save',
    'informations'  => 'Informations',
    'name'          => 'Title',
    'description'          => 'Description',
    'price'          => 'Price',
    'stock'          => 'Stock',
    'image'          => 'Image',
    'successadd'    => 'The Order has been correctly added.',
    'successmod'    => 'The Order has been correctly modified.',
    'newpassword'   => 'Your password has been saved.',
    'create'        => [
        'title'     => 'Add a Order',
    ],
    'edit' => [
        'title'     => 'Edit a Order',
    ],
    'list' => [
        'title'         => 'Order list',
        'id'            => 'Id',
        'name'          => 'Name',
        'creationdate'  => 'Creation date',
        'confirmdelete' => 'Do you confirm that you want to delete this Order ?',
        'deletesuccess' => 'The Order has been correctly deleted',
        'deleteerror'   => 'An error occured when trying to delete the Order',
    ],
    'client' => [
        'name'     => 'Client name',
        'phone'     => 'Phone number',
    ],
];
