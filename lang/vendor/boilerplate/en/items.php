<?php

return [
    'title'         => 'Items',
    'returntolist'  => 'Item list',
    'save'          => 'Save',
    'informations'  => 'Informations',
    'name'          => 'Title',
    'description'          => 'Description',
    'price'          => 'Price',
    'stock'          => 'Stock',
    'image'          => 'Image',
    'successadd'    => 'The Item has been correctly added.',
    'successmod'    => 'The Item has been correctly modified.',
    'newpassword'   => 'Your password has been saved.',
    'create'        => [
        'title'     => 'Add a Item',
    ],
    'edit' => [
        'title'     => 'Edit a Item',
    ],
    'list' => [
        'title'         => 'Item list',
        'id'            => 'Id',
        'name'          => 'Name',
        'creationdate'  => 'Creation date',
        'confirmdelete' => 'Do you confirm that you want to delete this Item ?',
        'deletesuccess' => 'The Item has been correctly deleted',
        'deleteerror'   => 'An error occured when trying to delete the Item',
    ],
];
